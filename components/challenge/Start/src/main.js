import Vue from 'vue';
import App from './App.vue';

export const eventBus = new Vue({
  methods: {
    updateServerStatus(server, status) {
      this.$emit('updateServerStatus', {...server, status});
    },
  },
});

new Vue({
  el: '#app',
  render: h => h(App),
});
