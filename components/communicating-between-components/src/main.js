import Vue from 'vue';
import App from './App.vue';

// KEY: YOU HAVE TO DO THIS BEFORE YOU CREATE YOUR VUE INSTANCE
export const eventBus = new Vue({
  methods: {
    changeAge(age) {
      this.$emit('ageWasEdited', age);
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
});
