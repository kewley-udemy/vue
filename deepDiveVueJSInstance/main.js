const data = {
    title: 'The VueJS Instance',
    showParagraph: false,
};

// Reusable Component we can reuse all over
Vue.component('hello', {
    template: '<h1>Hello!</h1>',
});

// instance is stored in variables
const vm1 = new Vue({
    data: data,
    methods: {
        show: function () {
            this.showParagraph = true;
            this.updateTitle('The VueJS Instance (Updated)');

            // will map JS objects to the "ref" keys defined in the html
            console.log(this.$refs);

            // how we can access our html elements
            this.$refs.myButton.innerText = 'Test';
        },
        updateTitle: function (title) {
            this.title = title;
        },
    },
    computed: {
        lowercaseTitle: function () {
            return this.title.toLowerCase();
        },
    },
    watch: {
        title: function (value) {
            alert('Title changed, new value: ' + value);
        },
    },
});

/**
 * The properties we see with $ are the "native" VueJS methods and properties we
 * can use.
 */
// Same as the el selector/property. Good if you don't know where you want to mount something
// or the element doesn't exist right away
vm1.$mount('#app1');

// Valid, but can NOT use this in your VueJS instance
vm1.newProp = 'New!';
// You will see getters/setters for the proxied properties, but, you won't see it for the 'newProp' above
console.log(vm1);

// Another way to access title
console.log(vm1.$data.title);

// They are the same
console.log(vm1.$data === data);

// You are able to interact with HTML elements via $refs. But if you change something this is NOT part of
// VueJS instance, so they may be overwritten by VueJS. Typically useful for when you need to get the current
// value of something and easier than using the querySelector.
vm1.$refs.heading.innerText = 'Something else';

// Access instance from outside
setTimeout(() => {
    // VueJS proxies the data on the Vue object, so we can just access it
    // like below
    vm1.title = 'Changed By Timer!';
    vm1.show();
}, 3000);

// Perfectly fine to do multiple instances, but note there is NO CONNECTION (KEY POINT TO MAKE)
// between the instances, think of them as two different VueJS instances separated your DOM.
// You will see this in production instances when you want to create widgets, for example, a calendar, or a tab Widget.
const vm2 = new Vue({
    el: '#app2',
    data: {
        title: 'The Second Instance',
    },
    methods: {
        onChange() {
            // access from another instance
            vm1.title = 'Changed!';
        },
    },
});

// We can create our own template as a string here
const vm3 = new Vue({
    template: `<h1>Hello!</h1>`,
});

// Raw vanilla JS
vm3.$mount(); // creates a Vue DOM element in the background, can access it via $el
document.getElementById('app3').appendChild(vm3.$el);

/**
 * Vue JS Lifecycles
 *
 * 1.) new Vue()
 * 2.) beforeCreate() -> Right after new, before initialize data & events
 * 3.) Data & events created
 * 4.) created() -> Instance created
 * 5.) compile template or el's template
 * 6.) beforeMount() -> Not visible just yet
 * 7.) Replace el with compiled template
 * 8.) Mounted to DOM
 * 9.) Loop
 *  a.) Data Changed
 *  b.) beforeUpdate()
 *  c.) Re-render DOM
 *  d.) updated*(
 * 10.) beforeDestroy() -> Right before destroy
 * 11.) Destroyed
 * 12.) destroyed() -> Right after
 */
const vm4 = new Vue({
    el: '#app4',
    data: {
        title: 'The VueJS Lifecycle Instance',
    },
    beforeCreate() {
        console.log('beforeCreate()');
    },
    created() {
        console.log('created()');
    },
    beforeMount() {
        console.log('beforeMount()');
    },
    mounted() {
        console.log('mounted()');
    },
    beforeUpdate() {
        console.log('beforeUpdate()');
    },
    updated() {
        console.log('updated()');
    },
    beforeDestroy() {
        console.log('beforeDestroy()');
    },
    destroyed() {
        console.log('destroyed()');
    },
    methods: {
        destroy() {
            this.$destroy();
        }
    }
});

