const healthBarClassBase = {
    backgroundColor: 'green',
    height: '1.9rem',
};

new Vue({
    el: '#app',
    data: {
        name: 'Monster Hunter!',
        player: {
            name: 'You',
            maxHealth: 100,
            damage: 0,
        },
        opponent: {
            name: 'Monster',
            maxHealth: 100,
            damage: 0,
        },
        gameStarted: false,
        events: [],
    },
    methods: {
        startGame() {
            this.player.damage = 0;
            this.opponent.damage = 0;
            this.events = [];
            this.gameStarted = true;
        },
        endGame() {
            this.player.damage = 0;
            this.opponent.damage = 0;
            this.events = [];
            this.gameStarted = false;
        },
        attack() {
            const playerAttackDamage = Math.floor(Math.random() * 10);
            const monsterAttackDamage = Math.floor(Math.random() * 10);
            this.player.damage += monsterAttackDamage;
            this.opponent.damage += playerAttackDamage;
            this.events = [
                {
                    opponent: true,
                    message: `${this.opponent.name} dealt ${monsterAttackDamage} damage to ${this.player.name}`,
                },
                {
                    player: true,
                    message: `${this.player.name} dealt ${playerAttackDamage} damage to ${this.opponent.name}`,
                },
                ...this.events,
            ];
        },
        specialAttack() {
            const playerAttackDamage = Math.floor(Math.random() * 25);
            const monsterAttackDamage = Math.floor(Math.random() * 10);
            this.player.damage += monsterAttackDamage;
            this.opponent.damage += playerAttackDamage;
            this.events = [
                {
                    opponent: true,
                    message: `${this.opponent.name} dealt ${monsterAttackDamage} damage to ${this.player.name}`,
                },
                {
                    player: true,
                    message: `${this.player.name} dealt a special attack of ${playerAttackDamage} to ${this.opponent.name}`,
                },
                ...this.events,
            ];
        },
        heal() {
            const playerHeal = Math.min(Math.floor(Math.random() * 10), this.player.damage);
            const monsterAttackDamage = Math.floor(Math.random() * 10);
            this.player.damage -= playerHeal;
            this.player.damage += monsterAttackDamage;
            this.events = [
                {
                    opponent: true,
                    message: `${this.opponent.name} dealt ${monsterAttackDamage} damage to ${this.player.name}`,
                },
                {
                    player: true,
                    message: `${this.player.name} healed ${playerHeal} health`,
                },
                ...this.events,
            ];
        },
        playerHealth() {
            return this.player.maxHealth - this.player.damage;
        },
        opponentHealth() {
            return this.opponent.maxHealth - this.opponent.damage;
        },
    },
    computed: {
        playerHealthClass() {
            return {
                ...healthBarClassBase,
                width: (this.playerHealth() / 4) + 'vw',
            };
        },
        opponentHealthClass() {
            return {
                ...healthBarClassBase,
                width: (this.opponentHealth() / 4) + 'vw',
            };
        },
        gameOver() {
            return this.player.damage >= this.player.maxHealth;
        },
        gameWon() {
            return this.opponent.damage >= this.opponent.maxHealth;
        },
    },
});