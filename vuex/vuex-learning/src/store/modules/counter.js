import * as types from '../types';

const state = {
  counter: 0,
};

const getters = {
  [types.DOUBLE_COUNTER](state) {
    return state.counter * 2;
  },
  [types.CLICK_COUNTER](state) {
    return state.counter + ' Clicks';
  },
};

const mutations = {
  increment(state, payload) {
    if (payload) {
      state.counter += payload;
    } else {
      state.counter++;
    }
  },
  decrement(state, payload) {
    if (payload) {
      state.counter -= payload;
    } else {
      state.counter--;
    }
  },
};

const actions = {
  // es6 destructure
  increment({commit}, payload) {
    commit('increment', payload);
  },
  // technically you get the whole context
  decrement(context, payload) {
    context.commit('decrement', payload);
  },
  asyncIncrement({commit}, {by, duration}) {
    setTimeout(() => commit('increment', by), duration);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
