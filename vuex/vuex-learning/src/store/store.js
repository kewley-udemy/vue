import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';
import actions from './actions'
import getters from './getters'
import mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value: 0,
  },
  getters,
  mutations,
  // Better to do this way, especially for asynchronous state changes
  // It's best practice to use actions to call upon mutations
  actions,
  modules: {
    counter
  }
});
